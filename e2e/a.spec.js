'use strict';

describe('The simple spec', function () {

  beforeEach(function () {
    browser.get('/index.html');
  });

  it('test the title', function() {
    //click the about button
    element(by.css('a.md-button[href="#about"]')).click();
    expect(element(by.css('h1')).getText()).toContain('About');
  });

  it('click on about message', function () {
    //go to about page
    element(by.css('a.md-button[href="#about"]')).click();
    //check to see that we don't have the message
    expect(element(by.css('#toast-container')).isPresent()).toBeFalsy();
    //click the message button
    element(by.css('button.md-button.md-ink-ripple.animated')).click();
    //test if the message is shown
    expect(element(by.css('#toast-container')).getText()).toContain('about');
    //close the message
    element(by.css('#toast-container')).click().then(function () { 
      expect(element(by.css('#toast-container')).isPresent()).toBeFalsy(); 
    });

  })

});