/**
 * Created by adu on 15/04/16.
 */
'use strict';

describe('about page', function () {

    beforeEach(function () {
        browser.get('/index.html');
    });

    it('test the title in the about page', function() {
        //click the about button
        element(by.css('a.md-button[href="#about"]')).click();
        expect(element(by.css('h1')).getText()).toContain('About');

        //click the message button
        element(by.css('button.md-button.md-ink-ripple.animated')).click();
        //test if the message is shown
        expect(element(by.css('#toast-container')).getText()).toContain('about');


        //close the message
        element(by.css('#toast-container')).click().then(function () {
            expect(element(by.css('#toast-container')).isPresent()).toBeFalsy();
        });

    });

});