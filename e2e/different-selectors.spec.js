'use strict';

describe('The simple spec', function () {

  beforeEach(function () {
    browser.get('/index.html');
  });

  // Find the element with ng-model="user" and type "jacksparrow" into it
  element(by.model('user')).sendKeys('jacksparrow');

  // Find the first (and only) button on the page and click it
  element(by.css(':button')).click();

  // Verify that there are 10 tasks
  expect(element.all(by.repeater('task in tasks')).count()).toEqual(10);

});
