'use strict';

describe('The simple spec', function () {

  beforeEach(function () {
    browser.get('/index.html');
  });

  it('should have the title', function() {
    expect(element(by.css('h1')).getText()).toBe('Code challenge AngularJs');
  });

});
