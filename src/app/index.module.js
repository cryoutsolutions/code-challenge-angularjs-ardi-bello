(function() {
  'use strict';

  angular
    .module('code-challenge-angularjs', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ui.router', 'ngMaterial', 'toastr']);

})();
