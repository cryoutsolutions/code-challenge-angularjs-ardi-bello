(function() {
  'use strict';

  angular
    .module('code-challenge-angularjs')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
