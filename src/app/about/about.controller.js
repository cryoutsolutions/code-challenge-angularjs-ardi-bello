(function() {
  'use strict';

  angular
    .module('code-challenge-angularjs')
    .controller('AboutController', AboutController);

  /** @ngInject */
  function AboutController($timeout, webDevTec, toastr) {
    var vm = this;

    vm.awesomeThings = [];
    vm.classAnimation = '';
    vm.creationDate = 1460706940262;
    vm.showToastr = showToastr;

    activate();

    function activate() {
      getWebDevTec();
    }

    function showToastr() {
      //setTimeout(function(){
        toastr.info('The about message',"Message",{
        timeOut: 0,
        extendedTimeOut: 0
        });
       //}, 3000);
      /*$timeout(function(){
        toastr.info('The about message',"Message",{
          timeOut: 0,
          extendedTimeOut: 0
        });
      },5000)*/
    }

    function getWebDevTec() {
      vm.awesomeThings = webDevTec.getTec();

      angular.forEach(vm.awesomeThings, function(awesomeThing) {
        awesomeThing.rank = Math.random();
      });
    }
  }
})();
