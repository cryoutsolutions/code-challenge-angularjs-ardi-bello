[Alt Tab](http://www.alttab.co)

# README #

Code challenge AngularJs

### What is this repository for? ###

* This is a code Challenge for AngularJs

### How do I get set up? ###

* Summary of set up

- npm install
- bower install
- npm install -g gulp

* How to run tests

Unit tests

- gulp test

E2E

- npm install -g protractor
- protractor

* Deployment instructions

### Contribution guidelines ###

* Writing tests

Unit tests are in each components folder and have spec.js extension

E2E tests are in the e2e folder

Unit or E2E tests are not mandatory but writing them is a big advantage

* Code review

Code will be reviews based on code quality, git workflow, general mediocre stuff, comments, variable naming

* Other guidelines

Git flow

For every challange create a new branch starting from that branch, make the specific changes then make a pull request to that branch


### Who do I talk to? ###

* adrian@alttab.co